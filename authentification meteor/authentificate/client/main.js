import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
// import { Accounts } from 'meteor/accounts-base';

import './main.html';





Template.register.events({
  'submit form': function (event, template) {
    event.preventDefault();
    var emailVar = template.find('#email').value;
    var passwordVar = template.find('#password').value;
    Accounts.createUser({
      email: emailVar,
      password: passwordVar
    });
  },
});

Template.login.events({
  'submit form': function (event, template) {
    event.preventDefault();
    var emailLogin = template.find('#emailLogin').value;
    var passwordLogin = template.find('#passwordLogin').value;
    Meteor.loginWithPassword(emailLogin, passwordLogin, function(errorObject){
      if(errorObject){
              alert("Echec de la connexion");
            }
    })
  },
});

Template.dashboard.events({
  'click .logout': function (event) {
    event.preventDefault();
    Meteor.logout();
  }
});

Template.dashboard.helpers({

});

Template.cars.helpers({
  cars: function () {
    cars = [];
    cars[0] = { marque: 'Twingo', couleur: "rouge", prix: "3000 euros", image: "twingo.jpg" }
    cars[1] = { marque: 'Peugeot', couleur: "bleu", prix: "10 000 euros", image: " img/peugeot.jpg" }
    cars[2] = { marque: 'Bugatti', couleur: "jaune", prix: "400 000 euros", image: "~/client/img/bugatti.jpg" }
    cars[3] = { marque: 'Aston Martin', couleur: "gris", prix: "200 000 euros", image: "aston-martin" }
    return cars;
  }
});